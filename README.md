This app has been archived. I'm leaving it up here because I like being able to look at old/bad code I've written. 

This project has no tests, almost no comments, and is a bit quirky to use. The version of this app I'm currently maintaining, is here, https://gitlab.com/Derek52/libreinvoice



# Old Readme

This is a relatively simple app for generating invoices. I built the gist of this in a weekend for my sister. I've been adding to it slowly over the past few weeks.

An android app basically boils down to 2 parts, the frontend, and the backend. At the current time, this project has most of the features I want, working, in one of the 2 parts. Few of the features, work in the frontend and backend. But, I'm working on that.

There is some heavy duty restructuring/refactoring that needs to be done, but I have been using the app to generate invoices, so it works enough at the moment.

I'd say I'm at version 0.4 or 0.5. When i get the features I want implemented, I plan to publish this on the playstore, and F-droid.
