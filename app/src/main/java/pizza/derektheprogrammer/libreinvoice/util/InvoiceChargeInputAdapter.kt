package pizza.derektheprogrammer.libreinvoice.util

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.InvoiceCharge

class InvoiceChargeInputAdapter(var inputCharges : List<InvoiceCharge>, val context : Context) : RecyclerView.Adapter<InvoiceChargeInputAdapter.InputChargeViewHolder>(){

    lateinit var clickListener: ClickListener

    fun setOnItemClickListener(listener : ClickListener) {
        clickListener = listener
    }

    override fun onCreateViewHolder(parent : ViewGroup, position : Int) : InputChargeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_invoice_charge_input, parent, false)
        return InputChargeViewHolder(view)
    }

    override fun onBindViewHolder(holder : InputChargeViewHolder, position : Int) {
        holder.inputChargeTitleView.addTextChangedListener (object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                inputCharges[position].description = p0.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        holder.inputChargeRateView.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.toString().isEmpty()) {
                    inputCharges[position].rate = p0.toString().toDouble()//I feel like there is a better way to do this
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        holder.inputChargeQtyView.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (!p0.toString().isEmpty()) {
                    inputCharges[position].qty = p0.toString().toDouble()
                }
            }
        })

    }

    override fun getItemCount() : Int {
        return inputCharges.size
    }

    fun addNewCharge(updatedChargeList : ArrayList<InvoiceCharge>) {
        inputCharges = updatedChargeList
        notifyDataSetChanged()
    }

    inner class InputChargeViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        internal val inputChargeTitleView = view.findViewById<EditText>(R.id.item_invoiceChargeTitle)
        internal val inputChargeRateView = view.findViewById<EditText>(R.id.item_invoiceChargeRate)
        internal val inputChargeQtyView = view.findViewById<EditText>(R.id.item_invoiceChargeQty)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view: View) {
            clickListener.onItemClick(adapterPosition, view)
        }

        override fun onLongClick(view: View): Boolean {
            clickListener.onItemLongClick(adapterPosition, view)
            return true
        }
    }

}
