package pizza.derektheprogrammer.libreinvoice.util

import android.arch.lifecycle.ViewModel
import pizza.derektheprogrammer.libreinvoice.database.InvoiceCharge

class InvoiceDataModel : ViewModel() {

    var clientName = ""
    var clientEmail = ""
    var invoiceDate = ""
    var invoiceNumber = ""

    var charges = List<InvoiceCharge>(1) {InvoiceCharge("", 0.0, 0.0, 0.0, 0)}

}