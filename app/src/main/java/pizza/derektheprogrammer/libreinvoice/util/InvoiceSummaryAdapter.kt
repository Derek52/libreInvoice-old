package pizza.derektheprogrammer.libreinvoice.util

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.Invoice

class InvoiceSummaryAdapter(private val invoices : List<Invoice>) : RecyclerView.Adapter<InvoiceSummaryAdapter.InvoiceViewHolder>(){

    lateinit var clickListener : ClickListener

    fun setOnItemClickListener(listener : ClickListener) {
        clickListener = listener
    }

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : InvoiceViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_invoice_summary, parent, false)
        return InvoiceViewHolder(view)
    }

    override fun onBindViewHolder(holder : InvoiceViewHolder, position : Int) {
        val invoice = invoices[position]

        holder.clientView.text = invoice.invoiceClientName
        holder.titleView.text = invoice.invoiceTitle
        holder.dateView.text = invoice.invoiceDate
        holder.amountView.text = invoice.invoiceTotalCharge.toString()

        /*holder.newInvoiceButton.setOnClickListener {
            val newInvoiceActivity = Intent(context, InvoiceActivity::class.java)
            newInvoiceActivity.putExtra("clientName", client.clientName)
            newInvoiceActivity.putExtra("clientEmail", client.clientEmail)
            context.startActivity(newInvoiceActivity)
        }*/
    }

    override fun getItemCount() : Int {
        return invoices.size
    }

    inner class InvoiceViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        internal val clientView = view.findViewById<TextView>(R.id.invoiceItemClientView)
        internal val titleView = view.findViewById<TextView>(R.id.invoiceItemTitleView)
        internal val dateView = view.findViewById<TextView>(R.id.invoiceItemDateView)
        internal val amountView = view.findViewById<TextView>(R.id.invoiceItemTotalAmountView)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view : View) {
            clickListener.onItemClick(adapterPosition, view)
        }

        override fun onLongClick(view : View) : Boolean {
            clickListener.onItemLongClick(adapterPosition, view)
            return true
        }
    }


}
