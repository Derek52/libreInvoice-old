package pizza.derektheprogrammer.libreinvoice.util

import android.content.Context
import android.content.Intent
import android.speech.tts.TextToSpeech
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.activities.InvoiceActivity
import pizza.derektheprogrammer.libreinvoice.database.Client

class ClientAdapter(private val clients : List<Client>, private val context : Context) : RecyclerView.Adapter<ClientAdapter.ClientViewHolder>() {

    lateinit var clickListener : ClickListener

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : ClientViewHolder{
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_client, parent, false)
        return ClientViewHolder(view)
    }

    override fun onBindViewHolder(holder : ClientViewHolder, position : Int) {
        val client = clients[position]

        holder.nameView.text = client.clientName
        holder.emailView.text = client.clientEmail
        holder.numberView.text = client.clientNumber

        holder.newInvoiceButton.setOnClickListener {
            val newInvoiceActivity = Intent(context, InvoiceActivity::class.java)
            newInvoiceActivity.putExtra("clientName", client.clientName)
            newInvoiceActivity.putExtra("clientEmail", client.clientEmail)
            context.startActivity(newInvoiceActivity)
        }
    }

    override fun getItemCount() : Int {
        return clients.size
    }

    fun setOnItemClickListener(listener : ClickListener) {
        clickListener = listener
    }

    inner class ClientViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        internal val nameView = view.findViewById<TextView>(R.id.itemClientName)
        internal val emailView = view.findViewById<TextView>(R.id.itemClientEmail)
        internal val numberView = view.findViewById<TextView>(R.id.itemClientNumber)
        internal val newInvoiceButton = view.findViewById<TextView>(R.id.clientItemNewInvoice)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view : View) {
            clickListener.onItemClick(adapterPosition, view)
        }

        override fun onLongClick(view : View) : Boolean {
            clickListener.onItemLongClick(adapterPosition, view)
            return true
        }
    }
}
