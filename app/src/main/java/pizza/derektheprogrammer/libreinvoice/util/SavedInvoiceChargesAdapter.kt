package pizza.derektheprogrammer.libreinvoice.util

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.InvoiceCharge

class SavedInvoiceChargesAdapter(private val invoiceCharges : List<InvoiceCharge>) : RecyclerView.Adapter<SavedInvoiceChargesAdapter.SavedInvoiceChargeViewHolder>() {

    lateinit var clickListener: ClickListener

    fun setOnItemClickListener(listener : ClickListener) {
        clickListener = listener
    }

    override fun onCreateViewHolder(parent : ViewGroup, position : Int) : SavedInvoiceChargeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_invoice_charge_data, parent, false)
        return SavedInvoiceChargeViewHolder(view)
    }

    override fun onBindViewHolder(holder: SavedInvoiceChargeViewHolder, position: Int) {
        val invoiceCharge = invoiceCharges[position]

        holder.chargeTitleDataView.text = invoiceCharge.description
        holder.chargeRateDataView.text = invoiceCharge.rate.toString()
        holder.chargeQtyDataView.text = invoiceCharge.qty.toString()
    }

    override fun getItemCount(): Int {
        return invoiceCharges.size
    }

    inner class SavedInvoiceChargeViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        internal val chargeTitleDataView = view.findViewById<TextView>(R.id.item_invoiceChargeTitleDataView)
        internal val chargeRateDataView = view.findViewById<TextView>(R.id.item_invoiceChargeRateDataView)
        internal val chargeQtyDataView = view.findViewById<TextView>(R.id.item_invoiceChargeQtyDataView)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view : View) {
            clickListener.onItemClick(adapterPosition, view)
        }

        override fun onLongClick(view : View) : Boolean {
            clickListener.onItemLongClick(adapterPosition, view)
            return true
        }
    }
}