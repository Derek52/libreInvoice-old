package pizza.derektheprogrammer.libreinvoice.util

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.InvoiceCharge

class InvoiceChargeDisplayAdapter(private var charges : List<InvoiceCharge>) : RecyclerView.Adapter<InvoiceChargeDisplayAdapter.DisplayChargeViewHolder>() {

    lateinit var clickListener : ClickListener

    fun setOnItemClickListener(listener : ClickListener) {
        clickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType : Int): DisplayChargeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_invoice_charge_display, parent, false)
        return DisplayChargeViewHolder(view)
    }

    override fun onBindViewHolder(holder: DisplayChargeViewHolder, position: Int) {
        val charge = charges[position]

        holder.chargeTitleView.text = charge.description
        holder.chargeRateView.text = formatDouble(charge.rate, false)
        holder.chargeQtyView.text = formatDouble(charge.qty, true)
        holder.chargeAmountView.text = formatDouble(charge.total, false)
    }

    private fun formatDouble(double : Double, stripZeroes : Boolean) : String{
        if (double - double.toInt() == 0.0 && stripZeroes) {
            return double.toInt().toString()
        } else {
            return String.format("%.2f", double)
        }
    }

    override fun getItemCount(): Int {
        return charges.size
    }

    fun getTotalCharge() : String {
        var totalCharge = 0f
        for (charge in charges) {
            totalCharge += charge.total.toFloat()
        }
        return String.format("%.2f", totalCharge)
    }

    fun updateCharges(newCharges : List<InvoiceCharge>) {
        charges = newCharges
        notifyDataSetChanged()
    }

    inner class DisplayChargeViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        internal val chargeTitleView = view.findViewById<TextView>(R.id.chargeTitleView)
        internal val chargeRateView = view.findViewById<TextView>(R.id.chargeRateView)
        internal val chargeQtyView = view.findViewById<TextView>(R.id.chargeQtyView)
        internal val chargeAmountView = view.findViewById<TextView>(R.id.chargeAmountView)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view : View) {
            clickListener.onItemClick(adapterPosition, view)
        }

        override fun onLongClick(view : View) : Boolean {
            clickListener.onItemLongClick(adapterPosition, view)
            return true
        }
    }
}