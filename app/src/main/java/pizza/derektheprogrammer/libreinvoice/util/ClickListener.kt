package pizza.derektheprogrammer.libreinvoice.util

import android.view.View

interface ClickListener {
    fun onItemClick(position : Int, view : View)
    fun onItemLongClick(position: Int, view : View)
}