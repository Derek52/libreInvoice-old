package pizza.derektheprogrammer.libreinvoice.fragments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class NewInvoicePagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position : Int) : Fragment {
        return when(position) {
            0 -> InputFragment()
            else -> DisplayInvoiceFragment()
        }
    }

    override fun getCount() : Int {
        return 2
    }

}