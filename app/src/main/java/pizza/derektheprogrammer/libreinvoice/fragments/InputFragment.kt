package pizza.derektheprogrammer.libreinvoice.fragments

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_input.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.InvoiceCharge
import pizza.derektheprogrammer.libreinvoice.util.InvoiceChargeInputAdapter
import pizza.derektheprogrammer.libreinvoice.util.InvoiceDataModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class InputFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val model = ViewModelProviders.of(activity!!).get(InvoiceDataModel::class.java)

        val df = DateFormat.getDateInstance(DateFormat.SHORT)
        val currentDate = df.format(Date())
        invoiceDateInput.setText(currentDate)

        clientNameInput.setText(model.clientName)
        clientEmailInput.setText(model.clientEmail)

        val inputCharges = ArrayList<InvoiceCharge>()
        inputCharges.add(InvoiceCharge("", 0.0, 0.0, 0.0, 0))

        val inputChargeAdapter = InvoiceChargeInputAdapter(inputCharges, activity!!.applicationContext)

        invoiceChargeInputRecyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(activity)
        invoiceChargeInputRecyclerView.layoutManager = layoutManager
        invoiceChargeInputRecyclerView.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        invoiceChargeInputRecyclerView.itemAnimator = DefaultItemAnimator()
        invoiceChargeInputRecyclerView.adapter = inputChargeAdapter

        inputAddChargeButton.setOnClickListener {
            //inputCharges.add(emptyCharge)
            inputCharges.add(InvoiceCharge("", 0.0, 0.0, 0.0, 0))
            activity!!.runOnUiThread {
                inputChargeAdapter.addNewCharge(inputCharges)
            }
        }

        updateInvoiceButton.setOnClickListener {
            model.clientName = clientNameInput.text.toString()
            model.clientEmail = clientEmailInput.text.toString()
            model.invoiceNumber = invoiceNumberInput.text.toString()
            model.invoiceDate = invoiceDateInput.text.toString()
            for (charge in inputCharges) {
                charge.total = charge.rate * charge.qty
            }
            model.charges = inputCharges
        }
    }
}