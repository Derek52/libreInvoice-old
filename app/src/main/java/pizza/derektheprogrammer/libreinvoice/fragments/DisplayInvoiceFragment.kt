package pizza.derektheprogrammer.libreinvoice.fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_display_invoice.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.*
import pizza.derektheprogrammer.libreinvoice.util.ClickListener
import pizza.derektheprogrammer.libreinvoice.util.InvoiceChargeDisplayAdapter
import pizza.derektheprogrammer.libreinvoice.util.InvoiceDataModel
import pizza.derektheprogrammer.libreinvoice.util.InvoiceSummaryAdapter
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.concurrent.thread

class DisplayInvoiceFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_display_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs = activity!!.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE)
        val dataModel = ViewModelProviders.of(activity!!).get(InvoiceDataModel::class.java)
        val db = LibreRoom.getDatabse(activity!!.applicationContext)
        val invoiceDao = db.invoiceDao()
        val invoiceChargeDao = db.invoiceChargeDao()

        val companyName = prefs.getString("companyName", "Company Name")
        val companyEmail = prefs.getString("companyEmail", "Companyemail@example.com")
        val companyPhoneNumber = "P: " + prefs.getString("companyPhoneNumber", "912-867-5309")
        val checkPayableName = prefs.getString("checkPayableName", "Captain Spock")
        val companyBankNumber = prefs.getString("companyBankNumber", "113434727")

        companyNameView.text = companyName
        companyEmailView.text = companyEmail
        companyPhoneNumberView.text = companyPhoneNumber
        checkPaymentInstructionsView.text = checkPayableName
        companyBankNumberView.text = companyBankNumber

        val companyLogoFilePath = prefs.getString("logoFilePath", "")
        //if the filepath is empty, there is no logo yet, hide the image view
        if (companyLogoFilePath.isEmpty()) {
            companyLogoView.visibility = View.INVISIBLE
        } else { //else, set the imageview to display our logo
            companyLogoView.visibility = View.VISIBLE
            try {
                val bitmap = BitmapFactory.decodeFile(companyLogoFilePath)
                companyLogoView.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }


        val invoiceChargeDisplayAdapter = InvoiceChargeDisplayAdapter(dataModel.charges)
        /*invoiceChargeDisplayAdapter.setOnItemClickListener(object : ClickListener {
            override fun onItemClick(position: Int, view: View) {
                Toast.makeText(activity!!.applicationContext, dataModel.charges[position].total.toString(), Toast.LENGTH_SHORT).show()
            }

            override fun onItemLongClick(position: Int, view: View) {
            }
        })*/

        invoiceTableRecyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(activity)
        invoiceTableRecyclerView.layoutManager = layoutManager
        invoiceTableRecyclerView.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        invoiceTableRecyclerView.itemAnimator = DefaultItemAnimator()
        invoiceTableRecyclerView.adapter = invoiceChargeDisplayAdapter

        companyLogoView.setOnClickListener {
            updateViews(invoiceChargeDisplayAdapter, dataModel)
        }

        emailInvoiceButton.setOnClickListener { v ->
            emailInvoiceButton.visibility = View.GONE
            thread {
                saveInvoiceToDb(invoiceDao, invoiceChargeDao)
            }

            try {
                val content = view.findViewById<View>(R.id.displayFragmentLayout)
                val bitmap = Bitmap.createBitmap(content.width, content.height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                content.draw(canvas)

                val outputFileName = "Invoice${dataModel.invoiceNumber}.png"
                val outputFilePath = File(Environment.getExternalStorageDirectory(), outputFileName)

                outputFilePath.createNewFile()
                val out = FileOutputStream(outputFilePath)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                out.close()

                val emailIntent = Intent(Intent.ACTION_SEND)
                //val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null))
                emailIntent.type = "text/plain"
                emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(dataModel.clientEmail))
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Invoice for $companyName")
                emailIntent.putExtra(Intent.EXTRA_TEXT, "This is an invoice")
                val uri = Uri.fromFile(outputFilePath)
                emailIntent.putExtra(Intent.EXTRA_STREAM, uri)
                startActivity(Intent.createChooser(emailIntent, "send email....."))

                //Toast.makeText(activity, "done writing png", Toast.LENGTH_SHORT).show()
            } catch (e: IOException) {
                Toast.makeText(activity, "$e", Toast.LENGTH_LONG).show()
                //e.printStackTrace()
            }
            emailInvoiceButton.visibility = View.VISIBLE
        }
    }

    private fun updateViews(chargeAdapter : InvoiceChargeDisplayAdapter, dataModel : InvoiceDataModel) {

        val clientName = dataModel.clientName
        val clientEmail = dataModel.clientEmail
        val invoiceNumber = dataModel.invoiceNumber
        val invoiceTitle = "INVOICE INV$invoiceNumber"
        val invoiceDate = dataModel.invoiceDate

        clientNameView.text = clientName
        clientEmailView.text = clientEmail
        invoiceDateView.text = invoiceDate
        invoiceNumberView.text = invoiceTitle

        val chargesToPost = ArrayList<InvoiceCharge>()
        for (charge in dataModel.charges) {
            chargesToPost.add(charge)
        }

        val newCharges : List<InvoiceCharge> = chargesToPost

        activity!!.runOnUiThread {
            chargeAdapter.updateCharges(newCharges)
        }

        val totalChargeAmount = chargeAdapter.getTotalCharge()
        val totalString = "Total\t\t$$totalChargeAmount"
        val balanceString = "Balance Due\t\t$$totalChargeAmount"

        totalAmountView.text = totalString
        balanceView.text = balanceString


    }

    private fun saveInvoiceToDb(invoiceDao : InvoiceDao, invoiceChargeDao : InvoiceChargeDao) {
        val dataModel = ViewModelProviders.of(activity!!).get(InvoiceDataModel::class.java)
        val newInvoice = Invoice(false, dataModel.invoiceNumber,
                dataModel.invoiceDate, 7, dataModel.clientName, dataModel.clientEmail, 100.00)
        for (charge in dataModel.charges) {
            charge.invoiceId = newInvoice.id
            println("saving charge to invoice with id equal to ${charge.invoiceId}")
            invoiceChargeDao.insert(charge)
        }
        invoiceDao.insert(newInvoice)

        println("Done saving invoice to Room")
    }

}