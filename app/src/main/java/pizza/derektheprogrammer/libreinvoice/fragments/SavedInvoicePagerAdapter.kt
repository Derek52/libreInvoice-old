package pizza.derektheprogrammer.libreinvoice.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class SavedInvoicePagerAdapter(fm : FragmentManager, val bundle : Bundle) : FragmentPagerAdapter(fm) {

    override fun getItem(position : Int) : Fragment {
        when(position) {
            0 -> {
                val savedInvoiceDataFragment = SavedInvoiceDataFragment()
                savedInvoiceDataFragment.arguments = bundle
                return savedInvoiceDataFragment
            }
            else -> return DisplayInvoiceFragment()
        }
    }

    override fun getCount() : Int {
        return 2
    }

}