package pizza.derektheprogrammer.libreinvoice.fragments

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_saved_invoice_data.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.Invoice
import pizza.derektheprogrammer.libreinvoice.database.InvoiceCharge
import pizza.derektheprogrammer.libreinvoice.database.LibreRoom
import pizza.derektheprogrammer.libreinvoice.util.InvoiceDataModel
import pizza.derektheprogrammer.libreinvoice.util.SavedInvoiceChargesAdapter
import kotlin.concurrent.thread

class SavedInvoiceDataFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_saved_invoice_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = ViewModelProviders.of(activity!!).get(InvoiceDataModel::class.java)

        val db = LibreRoom.getDatabse(activity!!.applicationContext)
        val invoiceDao = db.invoiceDao()
        val chargesDao = db.invoiceChargeDao()

        var invoice = Invoice(false, "", "", 7, "", "", 50.00)
        var invoiceCharges = List(1) {InvoiceCharge("", 0.0, 0.0, 0.00, 0)}

        val invoiceId = arguments!!.getLong("invoiceId", 0)

        var done = false
        thread {
            invoice = invoiceDao.findInvoiceById(invoiceId)
            invoiceCharges = chargesDao.findChargesForInvoice(invoiceId)
            done = true
        }

        while (!done) {

        }


        invoiceDateDataView.text = invoice.invoiceDate
        clientNameDataView.text = invoice.invoiceClientName
        clientEmailDataView.text = invoice.invoiceClientEmail
        invoiceNumberDataView.text = invoice.invoiceTitle


        val savedInvoiceAdapter = SavedInvoiceChargesAdapter(invoiceCharges)

        savedInvoiceDataRecyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(activity!!)
        savedInvoiceDataRecyclerView.layoutManager = layoutManager
        savedInvoiceDataRecyclerView.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        savedInvoiceDataRecyclerView.itemAnimator = DefaultItemAnimator()
        savedInvoiceDataRecyclerView.adapter = savedInvoiceAdapter

        model.invoiceDate = invoice.invoiceDate
        model.invoiceNumber = invoice.invoiceTitle
        model.clientName = invoice.invoiceClientName
        model.clientEmail = invoice.invoiceClientEmail
        model.charges = invoiceCharges

    }
}