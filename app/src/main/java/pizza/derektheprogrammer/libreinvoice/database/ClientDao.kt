package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface ClientDao {

    @get:Query("SELECT * FROM clients ORDER BY id ASC")
    val clients : List<Client>

    /*@Query("SELECT * FROM clients WHERE client_name LIKE :name OR client_name LIKE :name2")
    fun findClientByName(name : String, name2 : String) : List<Client>*/

    @Query("SELECT * FROM clients WHERE client_name LIKE :name")
    fun findClientByName(name : String) : List<Client>

    @Insert
    fun insert(client : Client)

    @Query("DELETE FROM clients")
    fun deleteAll()
}
