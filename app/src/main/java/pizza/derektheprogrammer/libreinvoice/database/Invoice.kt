package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "invoices")
class Invoice(@field:ColumnInfo(name = "invoice_paid")var paid : Boolean,
              @field:ColumnInfo(name = "invoice_title")val invoiceTitle : String,
              @field:ColumnInfo(name = "invoice_date")val invoiceDate : String,
              @field:ColumnInfo(name = "invoice_month")val invoiceMonth : Int,//this will be used to sort invoices by month, and it hasn't been implemented yet
              @field:ColumnInfo(name = "invoice_clientName")val invoiceClientName : String,
              @field:ColumnInfo(name = "invoice_clientEmail")val invoiceClientEmail : String,
              @field:ColumnInfo(name = "invoice_totalChargeAmount")val invoiceTotalCharge : Double) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Long = 0
}