package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

//this class name sucks. Room by itself felt weird, cuz Room is it's own thing. InvoiceRoom or ClientRoom seemed too specific, because this one class covers both.
//So I temporarily went with LibreRoom. Could be worse.

@Database(entities = arrayOf(Client::class, Invoice::class, InvoiceCharge::class), version = 1)
abstract class LibreRoom : RoomDatabase() {

    abstract fun clientDao() : ClientDao
    abstract fun invoiceDao() : InvoiceDao
    abstract fun invoiceChargeDao() : InvoiceChargeDao

    companion object {

        private var INSTANCE : LibreRoom? = null

        @JvmStatic
        fun getDatabse(context : Context) : LibreRoom {
            if (INSTANCE == null) {
                synchronized(LibreRoom::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext, LibreRoom::class.java, "libreinvoice_database").build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}