package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface InvoiceDao {

    @get:Query("SELECT * FROM invoices ORDER BY id ASC")
    val invoices : List<Invoice>

    @get:Query("SELECT * FROM invoices WHERE invoice_paid is 1")
    val paidInvoices : List<Invoice>

    @get:Query("SELECT * FROM invoices WHERE invoice_paid is 0")
    val unpaidInvoices : List<Invoice>

    @Query("SELECT * FROM invoices WHERE id is :id")
    fun findInvoiceById(id : Long) : Invoice

    @Query("SELECT * FROM invoices WHERE invoice_month LIKE :month")
    fun filterInvoicesByMonth(month : Int) : List<Invoice>

    @Query("SELECT * FROM invoices WHERE invoice_clientName LIKE :client")
    fun findInvoicesForClient(client : String) : List<Invoice>

    @Insert
    fun insert(invoice : Invoice)

    @Query("DELETE FROM invoices")
    fun deleteAll()
}