package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface InvoiceChargeDao {

    @Query("SELECT * FROM invoice_charges WHERE invoice_id IS :invoiceId")
    fun findChargesForInvoice(invoiceId : Long) : List<InvoiceCharge>

    @Insert
    fun insert(invoiceCharge : InvoiceCharge)

    @Query("DELETE FROM invoice_charges")
    fun deleteAll()
}