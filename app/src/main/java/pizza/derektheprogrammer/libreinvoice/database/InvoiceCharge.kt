package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "invoice_charges")
class InvoiceCharge(@field:ColumnInfo(name = "charge_description")var description : String,
                    @field:ColumnInfo(name = "charge_rate")var rate : Double,
                    @field:ColumnInfo(name = "charge_qty")var qty : Double,
                    @field:ColumnInfo(name = "charge_total")var total : Double,
                    @field:ColumnInfo(name = "invoice_id")var invoiceId : Long) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Long = 0
}