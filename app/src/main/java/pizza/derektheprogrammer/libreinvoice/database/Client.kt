package pizza.derektheprogrammer.libreinvoice.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "clients")
class Client(@field:ColumnInfo(name = "client_name")val clientName : String,
             @field:ColumnInfo(name = "client_email")val clientEmail : String,
             @field:ColumnInfo(name = "client_phone_number")val clientNumber : String) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id : Long = 0
}