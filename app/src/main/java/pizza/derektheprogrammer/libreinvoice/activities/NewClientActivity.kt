package pizza.derektheprogrammer.libreinvoice.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_new_client.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.Client
import pizza.derektheprogrammer.libreinvoice.database.ClientDao
import pizza.derektheprogrammer.libreinvoice.database.LibreRoom
import kotlin.concurrent.thread

class NewClientActivity : AppCompatActivity() {

    lateinit var clientDao : ClientDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_client)

        val db = LibreRoom.getDatabse(applicationContext)
        clientDao = db.clientDao()

        //var done = false
        newClientButton.setOnClickListener {
            val client = Client(clientNameInput.text.toString(), clientEmailInput.text.toString(), clientNumberInput.text.toString())
            saveClient(client)
            Toast.makeText(this, client.clientName, Toast.LENGTH_SHORT).show()
        }
    }

    private fun saveClient(client : Client) {
        //there's probably a better way to do this on a background thread, but this is simple and works, so I'm sticking with it for the moment.
        thread {
            clientDao.insert(client)
        }
    }
}
