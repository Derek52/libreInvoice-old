package pizza.derektheprogrammer.libreinvoice.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.Invoice
import pizza.derektheprogrammer.libreinvoice.database.LibreRoom
import pizza.derektheprogrammer.libreinvoice.util.ClickListener
import pizza.derektheprogrammer.libreinvoice.util.InvoiceSummaryAdapter
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            val newInvoiceIntent = Intent(this, InvoiceActivity::class.java)
            startActivity(newInvoiceIntent)
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        val db = LibreRoom.getDatabse(applicationContext)
        val invoiceDao = db.invoiceDao()

        val invoice = Invoice(false, "INV001", "07/21/18", 7, "Derek", "email", 40.00)

        val invoice2 = Invoice(true, "INV002", "07/21/18", 7, "Frank", "email", 402.00)

        var invoices = List<Invoice>(1) { invoice }


        var invoiceFilter = "all"
        intent.extras?.let {
            invoiceFilter = intent.extras.getString("invoiceFilter", "all")
        }

        var done = false
        thread {
            /*invoiceDao.deleteAll()
            invoiceDao.insert(invoice)
            invoiceDao.insert(invoice2)
            invoiceDao.insert(invoice)
            invoiceDao.insert(invoice2)*/

            when (invoiceFilter) {
                "paid" -> {
                    invoices = invoiceDao.paidInvoices
                }
                "unpaid" -> {
                    invoices = invoiceDao.unpaidInvoices
                }
                "all" -> {
                    invoices = invoiceDao.invoices
                }
                else -> {
                    invoices = invoiceDao.invoices
                }
            }

            done = true
        }

        while(!done) {

        }

        val invoiceAdapter = InvoiceSummaryAdapter(invoices)
        invoiceAdapter.setOnItemClickListener(object : ClickListener {
            override fun onItemClick(position: Int, view: View) {
                Toast.makeText(this@MainActivity, invoices[position].paid.toString(), Toast.LENGTH_SHORT).show()
                val loadInvoiceIntent = Intent(this@MainActivity, InvoiceActivity::class.java)
                loadInvoiceIntent.putExtra("loadingInvoice", true)
                startActivity(loadInvoiceIntent)
            }

            override fun onItemLongClick(position: Int, view: View) {
            }
        })

        mainInvoiceRecyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        mainInvoiceRecyclerView.layoutManager = layoutManager
        mainInvoiceRecyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        mainInvoiceRecyclerView.itemAnimator = DefaultItemAnimator()
        mainInvoiceRecyclerView.adapter = invoiceAdapter
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.drawer_client_item -> {
                val clientIntent = Intent(this, ClientListActivity::class.java)
                startActivity(clientIntent)
            }
            R.id.drawer_invoices -> {
                Toast.makeText(this, "Invoice", Toast.LENGTH_LONG).show()
            }
            R.id.drawer_paid_invoices -> {
                val thisIntent = Intent(this, MainActivity::class.java)
                thisIntent.putExtra("invoiceFilter", "paid")
                startActivity(thisIntent)
            }
            R.id.drawer_unpaid_invoices -> {
                val thisIntent = Intent(this, MainActivity::class.java)
                thisIntent.putExtra("invoiceFilter", "unpaid")
                startActivity(thisIntent)
            }
            R.id.drawer_company_info -> {
                val companyInfoIntent = Intent(this, CompanyInfoActivity::class.java)
                startActivity(companyInfoIntent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
