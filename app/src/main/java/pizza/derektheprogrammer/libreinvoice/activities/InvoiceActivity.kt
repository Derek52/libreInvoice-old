package pizza.derektheprogrammer.libreinvoice.activities

import android.annotation.TargetApi
import android.arch.lifecycle.ViewModelProviders
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_invoice.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.fragments.NewInvoicePagerAdapter
import pizza.derektheprogrammer.libreinvoice.fragments.SavedInvoicePagerAdapter
import pizza.derektheprogrammer.libreinvoice.util.InvoiceDataModel

class InvoiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice)

        if (shouldAskPermissions()) {
            askPermissions()
        }

        var loadingInvoice = false//this determines if we are creating a new invoice, or have come here to display an old one.

        intent.extras?.let {
            //if we are coming here from the client list activity, a client will have been passed as an intent extra.
            //this grabs that data, and stores it in our model so the fragments can access it.
            val model = ViewModelProviders.of(this).get(InvoiceDataModel::class.java)
            model.clientName = intent.extras.getString("clientName", "Derek")
            model.clientEmail = intent.extras.getString("clientEmail", "Briggs")
            //check and see if we passed the bool, saying we are loading an invoice
            loadingInvoice = intent.extras.getBoolean("loadingInvoice", false)

        }


        if (loadingInvoice) {
            val bundle = Bundle()
            bundle.putLong("invoiceId", 1)//pass the invoice id to the fragment, so it can search Room for that invoice's charges
            val savedInvoicePagerAdapter = SavedInvoicePagerAdapter(supportFragmentManager, bundle)
            invoiceViewPager.adapter = savedInvoicePagerAdapter
        } else {
            val invoicePagerAdapter = NewInvoicePagerAdapter(supportFragmentManager)
            invoiceViewPager.adapter = invoicePagerAdapter
        }
    }

    protected fun shouldAskPermissions() : Boolean {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1)
    }

    @TargetApi(23)
    protected fun askPermissions() {
        val permissions = arrayOf("android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE")
        val requestCode = 200
        requestPermissions(permissions, requestCode)
    }

}
