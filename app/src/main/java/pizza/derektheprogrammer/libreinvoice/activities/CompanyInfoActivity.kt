package pizza.derektheprogrammer.libreinvoice.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_company_info.*
import pizza.derektheprogrammer.libreinvoice.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class CompanyInfoActivity : AppCompatActivity() {

    private val PICK_IMAGE_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_info)
        //setSupportActionBar(toolbar)

        val prefs = this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE)

        val companyName = prefs.getString("companyName", "")
        if ( !companyName.isEmpty() ) {
            companyNameInput.setText(companyName)
        }

        val companyEmail = prefs.getString("companyEmail", "")
        if ( !companyEmail.isEmpty() ) {
            companyEmailInput.setText(companyEmail)
        }

        val companyPhoneNumber = prefs.getString("companyPhoneNumber", "")
        if ( !companyPhoneNumber.isEmpty() ) {
            companyPhoneNumberInput.setText(companyPhoneNumber)
        }

        val companyCheckName = prefs.getString("checkPayableName", "")
        if ( !companyCheckName.isEmpty() ) {
            companyCheckNameInput.setText(companyCheckName)
        }

        val companyBankNumber = prefs.getString("companyBankNumber", "")
        if ( !companyBankNumber.isEmpty() ) {
            companyBankNumberInput.setText(companyBankNumber)
        }

        val companyLogoFilePath = prefs.getString("logoFilePath", "")
        //Toast.makeText(this, companyLogoFilePath, Toast.LENGTH_LONG).show()
        //if the filepath is empty, there is no logo yet, hide the image view
        if ( companyLogoFilePath.isEmpty() ) {
            companyLogoSampleView.visibility = View.INVISIBLE
        } else { //else, set the imageview to display our logo
            companyLogoSampleView.visibility = View.VISIBLE
            try {
                val bitmap = BitmapFactory.decodeFile(companyLogoFilePath)
                companyLogoSampleView.setImageBitmap(bitmap)
            } catch (e : IOException) {
                e.printStackTrace()
            }

        }

        logoLinearLayout.setOnClickListener { view ->
            val pickLogoIntent = Intent()
            pickLogoIntent.type = "image/*"
            pickLogoIntent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(pickLogoIntent, "Select Invoice Logo"), PICK_IMAGE_REQUEST)
        }

        companyInfoFab.setOnClickListener { view ->
            with(prefs.edit()) {
                putString("companyName", companyNameInput.text.toString())
                putString("companyEmail", companyEmailInput.text.toString())
                putString("companyPhoneNumber", companyPhoneNumberInput.text.toString())
                putString("checkPayableName", companyCheckNameInput.text.toString())
                putString("companyBankNumber", companyBankNumberInput.text.toString())
                apply()
            }
            Toast.makeText(this, "Updated Company Info", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri = data.data

            val prefs = this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE)

            val outputFile = File(filesDir, "companyLogo.png") //used for writing the company logo, to our apps own internal storage.

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)

                outputFile.createNewFile()
                val out = FileOutputStream(outputFile)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                out.close()

                with(prefs.edit()) {
                    putString("logoFilePath", outputFile.absolutePath)
                    Toast.makeText(applicationContext, outputFile.absolutePath, Toast.LENGTH_SHORT).show()
                    apply()
                }

                companyLogoSampleView.setImageBitmap(bitmap)
            } catch (e : IOException) {
                e.printStackTrace()
            }
        }
    }
}
