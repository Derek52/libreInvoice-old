package pizza.derektheprogrammer.libreinvoice.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_client_list.*
import pizza.derektheprogrammer.libreinvoice.R
import pizza.derektheprogrammer.libreinvoice.database.Client
import pizza.derektheprogrammer.libreinvoice.database.ClientDao
import pizza.derektheprogrammer.libreinvoice.database.LibreRoom
import pizza.derektheprogrammer.libreinvoice.util.ClickListener
import pizza.derektheprogrammer.libreinvoice.util.ClientAdapter
import kotlin.concurrent.thread

class ClientListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client_list)

        val db = LibreRoom.getDatabse(applicationContext)
        val clientDao = db.clientDao()

        val client = Client("Four", "Ifixtechthings@gmail.com", "9122750668")

        var clientList = List(1) {client}

        var done = false
        thread {
            //clientList = clientDao.findClientByName("%d%")
            clientList = clientDao.clients
            done = true
        }
        while (!done) {

        }

        val clientAdapter = ClientAdapter(clientList, this)
        clientAdapter.setOnItemClickListener(object : ClickListener {
            override fun onItemClick(position: Int, view: View) {
                Toast.makeText(this@ClientListActivity, "Whateves", Toast.LENGTH_SHORT).show()
            }

            override fun onItemLongClick(position: Int, view: View) {
            }
        })

        clientRecyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        clientRecyclerView.layoutManager = layoutManager
        clientRecyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        clientRecyclerView.itemAnimator = DefaultItemAnimator()
        clientRecyclerView.adapter = clientAdapter

        clientListFab.setOnClickListener {
            val newClientIntent = Intent(this, NewClientActivity::class.java)
            startActivity(newClientIntent)
        }
    }
}
